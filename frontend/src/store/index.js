import Vue from 'vue'
import Vuex from 'vuex'
import UserApi from '@/api'

Vue.use(Vuex)
const api = new UserApi()

export default new Vuex.Store({
  state: {
    users: []
  },
  mutations: {
    setUsers (state, payload) {
      state.users = payload
    }
  },
  actions: {
    async getUsers (context) {
      const data = await api.getAllUsers()
      context.commit('setUsers', data.data.users)
    },
    async deleteUser (context, username) {
      return api.deleteUser(username)
    },
    async updateUser (context, data) {
      return api.updateUser(data)
    },
    async importUsersApi (context, data) {
      return api.importUsers(data)
    }
  }
})
