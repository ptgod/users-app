import axios from 'axios'

class UserApi {
  constructor () {
    this.api = axios.create({
      baseURL: 'http://0.0.0.0:8000'
    })
  }

  async getAllUsers () {
    const response = await this.api.get('/users')
    return response
  }

  async deleteUser (username) {
    const url = '/users/' + username
    const response = await this.api.delete(url)
    return response
  }

  async updateUser (data) {
    const url = '/users/' + data.username
    const formData = new FormData()
    formData.append('username', data.username)
    formData.append('first_name', data.first_name)
    formData.append('last_name', data.last_name)
    formData.append('email', data.email)
    formData.append('phone', data.phone)
    const response = await this.api.patch(url, formData)
    return response
  }

  async importUsers (data) {
    const response = await this.api.post('/users', data, { headers: { 'Content-Type': 'multipart/form-data' } })
    return response
  }
}

export default UserApi
