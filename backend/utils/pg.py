from pathlib import Path
from aiohttp.web_app import Application
from asyncpgsa import PG

DEFAULT_PG_URL = 'postgresql://postgres:password@postgres/app'
MAX_QUERY_ARGS = 32767
MAX_INTEGER = 2147483647

PROJECT_PATH = Path(__file__).parent.parent.resolve()


async def setup_pg(app: Application) -> PG:
    app['pg'] = PG()

    await app['pg'].init(
        DEFAULT_PG_URL,
        min_size=10,
        max_size=10
    )
    await app['pg'].fetchval('SELECT 1')

    try:
        yield
    finally:
        await app['pg'].pool.close()
