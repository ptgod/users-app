from aiohttp.web import run_app
from aiomisc import bind_socket

from api.app import create_app


def main():
    sock = bind_socket(address='0.0.0.0', port=8000,
                       proto_name='http')

    app = create_app()
    run_app(app, sock=sock)


if __name__ == '__main__':
    main()
