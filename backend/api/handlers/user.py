from http import HTTPStatus

from aiohttp.web_exceptions import HTTPNotFound
from aiohttp.web_response import Response

from api.handlers.base import BaseUserView


class UserView(BaseUserView):
    URL_PATH = r'/users/{username:\w+}'

    @property
    def username(self):
        return self.request.match_info.get('username')

    async def patch(self):
        data = await self.request.post()
        async with self.pg.transaction() as conn:
            user = await self.get_user_by_username(conn, self.username)
            if not user:
                raise HTTPNotFound()

            await self.update_user(conn, self.username, data)

            user = await self.get_user_by_username(conn, self.username)
        return Response(body={'data': user}, status=HTTPStatus.OK)

    async def delete(self):
        async with self.pg.transaction() as conn:
            user = await self.get_user_by_username(conn, self.username)
            if not user:
                raise HTTPNotFound()

            await self.delete_user_by_username(conn, self.username)

        return Response(body={'data': user}, status=HTTPStatus.OK)
