import aiohttp_cors
from aiohttp.web_urldispatcher import View
from asyncpgsa import PG

from api.handlers.query import UserQueryManager


class BaseUserView(View, aiohttp_cors.CorsViewMixin, UserQueryManager):
    URL_PATH: str

    @property
    def pg(self) -> PG:
        return self.request.app['pg']
