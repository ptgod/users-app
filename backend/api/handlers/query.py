from sqlalchemy import select

from db.schema import users_table

USERS_QUERY = select([
    users_table.c.user_id,
    users_table.c.username,
    users_table.c.first_name,
    users_table.c.last_name,
    users_table.c.email,
    users_table.c.phone
]).group_by(
    users_table.c.user_id,
)


class UserQueryManager(object):

    @staticmethod
    async def get_user_by_username(conn, username):
        query = USERS_QUERY.where(users_table.c.username == username)
        return await conn.fetchrow(query)

    @staticmethod
    async def get_all_users(pg):
        return await pg.fetch(USERS_QUERY.order_by('user_id'))

    @staticmethod
    async def get_all_users_by_usernames(pg, names):
        return await pg.fetch(USERS_QUERY.where(users_table.c.username.in_(names)))

    @staticmethod
    async def update_user(conn, username, data):
        values = {k: v if v != 'null' else None for k, v in data.items()}
        values['phone'] = int(values['phone']) if values['phone'] else None
        if values:
            query = users_table.update().values(values).where(users_table.c.username == username)
            await conn.execute(query)

    @staticmethod
    async def delete_user_by_username(conn, username):
        query = users_table.delete().where(users_table.c.username == username)
        await conn.execute(query)

    @staticmethod
    async def create_user(conn, data):
        values = {k: v for k, v in data.items()}

        if values:
            query = users_table.insert().values(values).returning(users_table.c.user_id)
            result = await conn.execute(query)
            return result
