import pandas as pd
from http import HTTPStatus

from aiomisc import chunk_list
from aiohttp.web_response import Response

from db.schema import users_table
from api.handlers.base import BaseUserView


class UsersView(BaseUserView):
    URL_PATH = r'/users'
    # Maximum number of lines to insert in postgres database by 1 request
    MAX_USERS_PER_INSERT = 32767 // len(users_table.columns)

    async def get(self):
        users = await self.get_all_users(pg=self.pg)
        return Response(body={'users': users}, status=HTTPStatus.OK)

    async def post(self):
        data = await self.request.post()
        data_frame = self.get_data_frame(data=data)

        if not self.headers_validation(data_frame=data_frame):
            return Response(status=HTTPStatus.OK, body={'error': {'type': 'headers_format'}})

        if not self.nan_data_validation(data_frame=data_frame):
            # no data in fields username/first_name/last_name
            return Response(status=HTTPStatus.OK, body={'error': {'type': 'nan_data_exist'}})

        users = data_frame.to_dict('records')
        unique_users = list(
            {v['username']: v
             for v in users}.values()
        )
        formatted_user_data = self.make_users_table_rows(users=unique_users)

        duplicated_usernames = await self.duplicated_username_validation(users=formatted_user_data)
        if duplicated_usernames:
            # usernames already exists in database
            return Response(status=HTTPStatus.OK, body={'error': {'type': 'duplicated_usernames',
                                                                  'duplicated_usernames': duplicated_usernames}})
        chunked_user_rows = chunk_list(formatted_user_data,
                                       self.MAX_USERS_PER_INSERT)
        users_query = users_table.insert()
        async with self.pg.transaction() as conn:
            for chunk in chunked_user_rows:
                await conn.execute(users_query.values(chunk))

        return Response(status=HTTPStatus.CREATED, body={})

    @staticmethod
    def headers_validation(data_frame):
        column_names = [name.lower().replace(' ', '') for name in data_frame.columns.ravel()]
        if sorted(column_names) == ['email', 'first_name', 'last_name', 'phone', 'username']:
            return True
        return False

    @staticmethod
    def nan_data_validation(data_frame):
        nan_in_username = data_frame['username'].isnull().values.any()
        nan_in_first_name = data_frame['first_name'].isnull().values.any()
        nan_in_last_name = data_frame['last_name'].isnull().values.any()

        if any((nan_in_username, nan_in_first_name, nan_in_last_name,)):
            return False
        return True

    async def duplicated_username_validation(self, users):
        from api.handlers.query import USERS_QUERY
        names = [user.get('username') for user in users]
        duplicated_users = await self.pg.fetch(USERS_QUERY.where(users_table.c.username.in_(names)))
        if not duplicated_users:
            return []

        duplicated_usernames = [dict(user).get('username') for user in duplicated_users]
        return duplicated_usernames

    @staticmethod
    def get_data_frame(data):
        file = data['file'].file
        file_format = data['file'].filename.split('.')[1]

        strip_converter = lambda x: x.strip() if x.strip() else None
        if file_format == 'csv':
            data_frame = pd.read_csv(file, sep=',', converters={
                'email': strip_converter, 'first_name': strip_converter,
                'last_name': strip_converter, 'phone': strip_converter,
                'username': strip_converter
            })
        else:
            # excel
            data_frame = pd.read_excel(io=file, sheet_name=0)

        return data_frame

    @staticmethod
    def make_users_table_rows(users):
        return [{
            'username': str(user['username']) if not pd.isnull(user['username']) else None,
            'first_name': str(user['first_name']) if not pd.isnull(user['first_name']) else None,
            'last_name': str(user['last_name']) if not pd.isnull(user['last_name']) else None,
            'email': str(user['email']) if not pd.isnull(user['email']) else None,
            'phone': int(user['phone']) if not pd.isnull(user['phone']) else None,
        } for user in users]
