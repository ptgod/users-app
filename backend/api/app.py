from types import MappingProxyType
from typing import Mapping

import aiohttp_cors
from aiohttp import PAYLOAD_REGISTRY
from aiohttp.web_app import Application
from aiohttp_apispec import validation_middleware

from api.handlers import HANDLERS
from api.middlewares import error_middleware

from utils.pg import setup_pg
from api.payloads import JsonPayload

# increase request size to aiohttp
MEGABYTE = 1024 ** 2
MAX_REQUEST_SIZE = 70 * MEGABYTE


def create_app() -> Application:
    app = Application(
        client_max_size=MAX_REQUEST_SIZE,
        middlewares=[error_middleware, validation_middleware]
    )

    # Connect at start to postgres and disconnect at stop
    app.cleanup_ctx.append(setup_pg)

    for handler in HANDLERS:
        app.router.add_route('*', handler.URL_PATH, handler)

    # Configure default CORS settings
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
    })
    # Configure CORS on all routes
    for route in list(app.router.routes()):
        cors.add(route)

    # Automatic serialization to json of data in HTTP responses
    PAYLOAD_REGISTRY.register(JsonPayload, (Mapping, MappingProxyType))
    return app
